<?php

namespace Shifft\p4;

use Illuminate\Support\ServiceProvider;

class P4ServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(){
        // Publish config file
        $this->publishes([
            __DIR__.'/config/p4.php' => config_path('p4.php')
        ], 'config');

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(){
        // Make sure the facade can be used by just having "use p4;" in files.
        $this->app->singleton('p4', function($app) {
            return new P4();
        });

        // Merge configfile
        $this->mergeConfigFrom(
            __DIR__.'/config/p4.php'
        , 'config');
    }
}
