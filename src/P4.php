<?php
declare(strict_types=1);
namespace Shifft\p4;

use stdClass;

class P4
{
    private static $p4Token;
    private static $p4TokenTime;
    private static $p4Key;
    private static $p4PubKeyDecoded;
    private static $p4Errors;

    /**
     * Login into the EnergyShifft P4 API
     *
     * @sets p4Token
     * @sets p4TokenTime
     */
    private static function p4Login()
	{
        if(static::$p4Key == null)
		{
            static::$p4Key = static::getKeystore(resource_path(config('p4.cert')), config('p4.password'));
            $pubKey = static::$p4Key['cert'];
            static::$p4PubKeyDecoded = base64_encode($pubKey);
        }

        $headers = array('AUTHORIZATIONKey: key='.static::$p4PubKeyDecoded);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, config('p4.url').'login/start');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        $output = curl_exec($ch);

        curl_close($ch);
        $data = '';
        openssl_private_decrypt (base64_decode($output), $data, static::$p4Key['pkey']);
        $data = json_decode($data);
        $sign = '';
        openssl_sign($data->challenge, $sign, static::$p4Key['pkey']);
        $sign = base64_encode($sign);

        $headers = array('AUTHORIZATIONKEY: key='.static::$p4PubKeyDecoded.', response="'.$sign.'"');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, config('p4.url').'login/response');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        $output = curl_exec($ch);
        curl_close($ch);
        $data = '';
        openssl_private_decrypt (base64_decode($output), $data, static::$p4Key['pkey']);
        $data = json_decode($data);

        static::$p4Token = $data->token;
        static::$p4TokenTime = time();

        return true;
    }

    /**
     * Function to get the Keystore. Needed to login into the P4 API.
     *
     * @param $keystoreFile
     * @param $keyPhrase
     * @return bool
     */
    private static function getKeystore($keystoreFile, $keyPhrase)
	{
        // Get the path to the keystore
        $keyStore = $keystoreFile;
        if(!file_exists($keyStore)){
            return false;
        }

        // Read the keystore file
        $pkcs12 = file_get_contents($keyStore);
        if(!$pkcs12)
		{
            return false;
        }

        // Get the keystore
        if(!openssl_pkcs12_read($pkcs12, $certs, $keyPhrase))
		{
            return false;
        }

        // Return the key store
        return $certs;
    }

    /**
     * Method to send a request to the P4 API
     *
     * @param $endPoint
     * @param $data
     * @param $method
     * @return mixed
     */
    public static function sendP4Request(string $endPoint, array $data, string $method)
	{
        static::$p4Errors = array();

        // Log in the API if needed
        if(static::$p4Token == null || static::$p4TokenTime + 300 < time())
		{
            static::p4Login();
        }

        $data = is_array($data) ? json_encode($data) : $data;
        $headers = array('AuthorizationToken: token="'.static::$p4Token.'"',
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, config('p4.url').$endPoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $data = '';

        if(static::is_json($output))
		{
            $tmp = json_decode($output);
            if(isset($tmp->number)){
                for($i = 1; $i <= $tmp->number; $i++)
				{
                    $element = 'part'.$i;
                    $data .= static::decrypte($tmp->parts->$element);
                }
            }
			else
			{
                $data = $output;
            }
        }
		else
		{
            $data = static::decrypte($output);
        }

        return 
		[
            'data'      =>  json_decode($data),
            'httpcode'  => $httpcode,
            'url'       => config('p4.url').$endPoint
        ];
    }

    /**
     * Function to check if a string is valid JSON
     *
     * @param $input
     * @return bool
     */
    private static function is_json($input)
	{
		if($input == null || is_bool($input))
		{
			return false;
		}
        $input = trim($input);
        if (substr($input,0,1)!='{' OR substr($input,-1,1)!='}')
            return false;

        return is_array(@json_decode($input, true));
    }

    /**
     * Method to decrypt data from the P4 api
     *
     * @param $input
     * @return string
     */
    private static function decrypte($input)
	{
        $data = '';
        openssl_private_decrypt(base64_decode($input), $data, static::$p4Key['pkey']);
        return $data;
    }
	
    /**
     *  @brief Adds a user to the P4 API and returns the result
     *  
     *  @param string $name the name
     *  @param string $postal the postal
     *  @param int $number the housenumber
     *  @param string $accept the accept date
     *  @param string $meterCode the smart meter code
     *  @param string $additional the housenumber additional
     *  @param array $eans a array with eans, in case a house has more then one ean for a certain type
     *  @return array
     */
    public static function addP4User(string $name, string $postal, int $number, string $accept, string $meterCode = null, string $additional = null, array $eans = []): ?array
	{
        $endPoint = 'persons';

        $data = array(
            'name'          => $name,
            'postal'        => strtoupper($postal),
            'number'        => $number,
            'additional'    => $additional,
            'accept'        => $accept,
            'code'          => $meterCode
        );
		foreach($eans as $type=>$ean)
		{
			if($type == 'eanElek' || $type == 'eanGas')
			{
				$data[$type] = $ean;
			}
		}

        $method = 'POST';
        return static::sendP4Request($endPoint, $data, $method);
	}

    /**
     * Get the User data that's known to the API
     *
     * @param int $p4_id
     * @return mixed
     */
    public static function getP4UserData(int $p4_id){
        $data = array();
        $endPoint = 'persons/'.$p4_id;
        $method = 'GET';

        return self::sendP4Request($endPoint, $data, $method);
    }

    /**
     * Remove a user from the P4 database.
     *
     * @param $p4Id
     * @return array
     */
    public static function removeUser(int $p4Id): array
	{
        $data = array();
        $method = 'DELETE';
        $endPoint = 'persons/'.$p4Id;

        return self::sendP4Request($endPoint, $data, $method);
    }

    /**
     * Get EnergyData from the P4 API.
     *
     * @param int $p4id
     * @param bool $date
     * @return array
     */
    public static function fetchEnergyData(int $p4id, string $date = null, string $endDate = null): array
	{
        if ($date == null)
		{
            $date = date('Y-m-d H:i:s' ,strtotime("-2 days"));
        }

        $data = array();
        $endPoint = 'persons/'.$p4id.'/energyData/start|'.date('Ymd0000', strtotime($date)).'/end|'.date('Ymd0000', strtotime($endDate != null ? $endDate : date('Y-m-d H:i:s', strtotime($date.' +1 days')))).'/interval|quarter';
        $method = 'GET';

        return static::sendP4Request($endPoint, $data, $method);
    }

    /**
     * Get EnergyData from the P4 API.
     *
     * @param int $p4id
     * @param bool $date
     * @return array
     */
    public static function fetchEnergyDataDay(int $p4id, string $date = null): array
	{
        if ($date == null)
		{
            $date = date('Y-m-d H:i:s' ,strtotime("-2 days"));
        }

        $data = array();
        $endPoint = 'persons/'.$p4id.'/energyDataDay/start|'.date('Ymd0000', strtotime($date)).'/end|'.date('Ymd0000', strtotime('+1 days', strtotime($date)));
        $method = 'GET';

        return static::sendP4Request($endPoint, $data, $method);
    }
}

?>
