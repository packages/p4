<?php

namespace Shifft\p4;

use Illuminate\Support\Facades\Facade;

class P4Facade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'P4';
    }
}
