## Shifft P4
#### What is this?
This package allows communication between Laravel and the (closed) 
p4 API, found at `p4.energyshifft.com`.

#### Getting Started
To get started, require the package with `composer require shifft/p4` and publish the configfile with `php artisan vendor:publish --tag=config`

Please modify the `config/p4.php` to match your configuration.

#### Usage
Have the following code at the top of your file
```php
use Shifft\p4\P4;
```

And use it as such:
```php
$result = P4::addP4User('Terry', '3042EC', 27, '02-03-2018', '0013616218', 'D');
$result = P4::fetchEnergyData(1);
```

